﻿using Wallet.Domain.Interfaces.MSSQL;
using Wallet.Infrastructure.Persistence.MSSQL.Context;

namespace Wallet.Infrastructure.Persistence.MSSQL.Repositories
{
    public class WalletRepository : GenericRepository<Domain.Entites.Wallet>, IWalletRepository
    {
        public WalletRepository(DataContext dataContext) : base(dataContext)
        {
        }
    }
}
