﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Wallet.Domain.Interfaces.MSSQL;
using Wallet.Infrastructure.Persistence.MSSQL.Context;

namespace Wallet.Infrastructure.Persistence.MSSQL.Repositories
{
    public class GenericRepository<Entity> : IGenericRepository<Entity> where Entity : class
    {
        private DataContext _dataContext;
        private DbSet<Entity> _dbSet;
        public GenericRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
            _dbSet = dataContext.Set<Entity>();
        }
        public void Create(Entity entity)
        {
            try
            {
                _dbSet.Add(entity);
            }
            catch (Exception) { throw; }
        }

        public void Delete(Entity entity)
        {
            try
            {
                _dbSet.Remove(entity);
            }
            catch (Exception) { throw; }
        }

        public async Task<Entity> ReadAsync(Expression<Func<Entity, bool>> expression)
        {
            try
            {
                return await _dbSet.FindAsync(expression);
            }
            catch (Exception) { throw; }
        }

        public void Update(Entity entity)
        {
            try
            {
                _dataContext.Entry(entity).State = EntityState.Modified;
            }
            catch (Exception) { throw; }
        }
    }
}
