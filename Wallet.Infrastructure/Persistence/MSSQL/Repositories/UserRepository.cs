﻿using Wallet.Domain.Entites;
using Wallet.Domain.Interfaces.MSSQL;
using Wallet.Infrastructure.Persistence.MSSQL.Context;

namespace Wallet.Infrastructure.Persistence.MSSQL.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(DataContext dataContext) : base(dataContext)
        {
        }
    }
}
