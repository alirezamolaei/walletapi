﻿using Wallet.Domain.Entites;
using Wallet.Domain.Interfaces.MSSQL;
using Wallet.Infrastructure.Persistence.MSSQL.Context;

namespace Wallet.Infrastructure.Persistence.MSSQL.Repositories
{
    public class PaymentLogRepository : GenericRepository<PaymentLog>, IPaymentLogRepository
    {
        public PaymentLogRepository(DataContext dataContext) : base(dataContext)
        {
        }
    }
}
