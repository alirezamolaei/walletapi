﻿using Wallet.Domain.Interfaces.MSSQL;
using Wallet.Infrastructure.Persistence.MSSQL.Context;
using Wallet.Infrastructure.Persistence.MSSQL.Repositories;

namespace Wallet.Infrastructure.Persistence.MSSQL.Uow
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Prop
        private readonly DataContext _dataContext;
        public IPaymentLogRepository PaymentLogs { get; }
        public IUserRepository Users { get; }
        public IWalletRepository Wallets { get; }
        #endregion

        #region Ctor
        public UnitOfWork(DataContext dataContext,
        IPaymentLogRepository paymentLogRepository,
        IUserRepository userRepository,
        IWalletRepository walletRepository)
        {
            _dataContext = dataContext;
            PaymentLogs = paymentLogRepository;
            Users = userRepository;
            Wallets = walletRepository;

        }
        #endregion

        #region Dispose 
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion


        public async Task SubmitChangeAsync()
        {
            try
            {
                await _dataContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
