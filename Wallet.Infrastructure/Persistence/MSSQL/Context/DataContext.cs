﻿using Microsoft.EntityFrameworkCore;
using Wallet.Domain.Entites;
using Wallet.Domain.ValueObjects.GlobalVars;

namespace Wallet.Infrastructure.Persistence.MSSQL.Context
{
    public class DataContext : DbContext
    {
        #region Prop
        public DbSet<User> Users { get; set; }
        public DbSet<Domain.Entites.Wallet> Wallets { get; set; }
        public DbSet<PaymentLog> PaymentLogs { get; set; }
        #endregion

        #region Ctor
        public DataContext(DbContextOptions<DataContext> option) : base(option)
        {
            Database.EnsureCreated();
        }
        #endregion



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(GlobalVars.ConnectionString);
            }
            base.OnConfiguring(optionsBuilder);
        }




    }
}
