﻿using Wallet.Domain.Entites;
using Wallet.Domain.Interfaces.Identity;
using Wallet.Domain.Interfaces.MSSQL;
using Wallet.Infrastructure.Identity.Jwt;

namespace Wallet.Infrastructure.Identity
{
    public class IdentityService : IIdentityService
    {
        readonly IUnitOfWork _unitOfWork;
        public IdentityService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<User?> AuthorizeAsync(string mobile)
        {
            try
            {
                var userInfo = await _unitOfWork.Users.ReadAsync(z => z.Mobile == mobile);
                if (userInfo is null) return null;
                userInfo.Token = JWTManager.Authenticate(userInfo);
                _unitOfWork.Users.Update(userInfo);
                return userInfo;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> CreateUserAsync(User user)
        {
            try
            {
                _unitOfWork.Users.Create(user);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> DeleteUserAsync(User user)
        {
            try
            {
                _unitOfWork.Users.Delete(user);
                return true;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<User> GetUserAsync(string mobile)
        {
            try
            {
                return await _unitOfWork.Users.ReadAsync(z => z.Mobile == mobile);

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
