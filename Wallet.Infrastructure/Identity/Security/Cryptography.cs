﻿using System.Security.Cryptography;
using System.Text;

namespace Wallet.Infrastructure.Identity.Security
{
    public class Cryptography
    {
        public string MD5Generate(string input)
        {
            try
            {
                var md5 = MD5.Create();
                return Convert.ToBase64String(md5.ComputeHash(Encoding.UTF8.GetBytes(input)));
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
