﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Wallet.Domain.Entites;
using Wallet.Domain.ValueObjects.GlobalVars;

namespace Wallet.Infrastructure.Identity.Jwt
{
    public class JWTManager
    {
        public static string Authenticate(User user)
        {
            try
            {
                var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(GlobalVars.SecretKey));
                var credential = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha512);
                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, user.Guid.ToString()));
                //claims.Add(new Claim(ClaimTypes.Role, "user"));

                var token = new JwtSecurityToken(
                    GlobalVars.ApiName,
                    GlobalVars.ApiName,
                    claims,
                    expires: DateTime.UtcNow.AddDays(GlobalVars.ExpireDay),
                    signingCredentials: credential
                );
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
