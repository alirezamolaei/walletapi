﻿using System.Text;
using Wallet.Domain.ValueObjects.GlobalVars;

namespace Wallet.Infrastructure.Files
{
    public static class TextFileBuilder
    {
        static readonly string _path = GlobalVars.Path;
        static object _lock = new();

        static TextFileBuilder()
        {
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);
        }
        public static void WriteText(string text)
        {
            try
            {
                lock (_lock)
                {
                    FileStream fileStream =
                        File.Open(Path.Combine(_path, DateTime.Now.ToString()), FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
                    StreamWriter writer = new(fileStream, Encoding.Unicode);
                    try
                    {
                        writer.Write(text);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        fileStream.Close();
                        writer.Close();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
