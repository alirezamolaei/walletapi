﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Wallet.Infrastructure.Persistence.MSSQL.Context;
using Wallet.Domain.Interfaces.Identity;
using Wallet.Infrastructure.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Wallet.Domain.ValueObjects.GlobalVars;
using Wallet.Domain.Interfaces.MSSQL;
using Wallet.Infrastructure.Persistence.MSSQL.Uow;
using Wallet.Infrastructure.Persistence.MSSQL.Repositories;
namespace Wallet.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {

            //security
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(GlobalVars.SecretKey)),
                        ValidateAudience = true,
                        ValidateIssuer = true,
                        ValidIssuer = GlobalVars.ApiName,
                        ValidAudience = GlobalVars.ApiName
                    };
                });

            services.AddCors(z =>
                z.AddPolicy("AllowOrigin", opt => opt.AllowAnyOrigin().AllowAnyHeader()));

            //database
            services.AddDbContext<DataContext>(z => z.UseSqlServer(GlobalVars.ConnectionString));

            //services
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPaymentLogRepository, PaymentLogRepository>();
            services.AddTransient<IWalletRepository, WalletRepository>();


            return services;
        }
    }
}
