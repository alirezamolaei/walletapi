using Microsoft.OpenApi.Models;
using Wallet.Infrastructure;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(z =>
{
    z.SwaggerDoc("v1", new OpenApiInfo() { Title = "WalletApi", Version = "v1.1", Description = "Test Api" });
    z.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        In = ParameterLocation.Header,
        Description = "Wellcome, Please Enter Token Bearer",
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey
    });
    z.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
        {
            new OpenApiSecurityScheme
            {
                Reference =new OpenApiReference(){
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },Array.Empty<string>()
        }
    });
});
builder.Services.AddMvc();
builder.Services.AddInfrastructure();//Add Infrastructure

var app = builder.Build();
app.UseSwagger();
app.UseSwaggerUI();
app.UseAuthorization();
app.MapControllers();
app.Run();
