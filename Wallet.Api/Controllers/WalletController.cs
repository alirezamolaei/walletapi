﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wallet.Application.Logics.WalletLogic;
using Wallet.Application.Utilitys.ApiResponse;
using Wallet.Domain.Interfaces.MSSQL;

namespace Wallet.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = "user")]
    public class WalletController : ControllerBase
    {
        readonly WalletLogicMethods _walletLogic;
        public WalletController(IUnitOfWork unitOfWork)
        {
            _walletLogic = new(unitOfWork);
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<Response<GetWalletResponse>> Wallet()
        {
            var userId = User?.Identity?.Name;
            return await _walletLogic.GetWallet(userId);

        }

        [HttpPatch]
        [Route("[action]")]
        public async Task<Response<bool>> Wallet(UpdateWalletRequest req)
        {
            var userId = User?.Identity?.Name;
            return await _walletLogic.UpdateWallet(req, userId);
        }
    }
}
