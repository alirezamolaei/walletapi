﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wallet.Application.Logics.UserLogic;
using Wallet.Application.Utilitys.ApiResponse;
using Wallet.Domain.Interfaces.Identity;

namespace Wallet.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class UserController : ControllerBase
    {
        UserLogicMethods _userLogic;
        public UserController(IIdentityService identityService)
        {
            _userLogic = new(identityService);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Response<SignInResponse>> SignIn(SignInRequest req)
        {
            return await _userLogic.SignInAsync(req);
        }
        /// <summary>
        /// Developing
        /// </summary>
        [HttpPost]
        [Route("[action]")]
        public void SignUp(SignUpRequest req)
        {
            _userLogic.SignUp(req);
        }
    }
}
