﻿namespace Wallet.Domain.Common
{
    public class BaseEntity
    {
        public Guid Guid { get; set; }
        public long Id { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
    }
}
