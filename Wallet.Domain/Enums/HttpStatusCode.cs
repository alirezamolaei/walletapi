﻿namespace Wallet.Domain.Enums
{
    public class HttpStatusCode
    {
        public enum StatusCodesEnum
        {
            Successes,
            NotFound,
            Forbidden,
            Exception,
        }
    }
}
