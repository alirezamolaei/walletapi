﻿using Wallet.Domain.Entites;

namespace Wallet.Domain.Interfaces.MSSQL
{
    public interface IPaymentLogRepository : IGenericRepository<PaymentLog>
    {

    }
}
