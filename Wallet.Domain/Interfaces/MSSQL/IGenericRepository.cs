﻿using System.Linq.Expressions;

namespace Wallet.Domain.Interfaces.MSSQL
{
    public interface IGenericRepository<Entity> where Entity : class
    {
        void Create(Entity entity);
        void Update(Entity entity);
        void Delete(Entity entity);
        Task<Entity> ReadAsync(Expression<Func<Entity, bool>> expression);

    }
}
