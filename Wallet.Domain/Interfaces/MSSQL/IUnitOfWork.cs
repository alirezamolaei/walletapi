﻿namespace Wallet.Domain.Interfaces.MSSQL
{
    public interface IUnitOfWork : IDisposable
    {
        IPaymentLogRepository PaymentLogs { get; }
        IUserRepository Users { get; }
        IWalletRepository Wallets { get; }

        Task SubmitChangeAsync();

    }
}
