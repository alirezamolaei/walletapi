﻿using Wallet.Domain.Entites;

namespace Wallet.Domain.Interfaces.MSSQL
{
    public interface IUserRepository : IGenericRepository<User>
    {

    }
}
