﻿using Wallet.Domain.Entites;

namespace Wallet.Domain.Interfaces.Identity
{
    public interface IIdentityService
    {
        Task<User?> GetUserAsync(string mobile);
        Task<bool> CreateUserAsync(User user);
        Task<bool> DeleteUserAsync(User user);
        Task<User> AuthorizeAsync(string mobile);
    }
}
