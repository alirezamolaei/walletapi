﻿namespace Wallet.Domain.ValueObjects.GlobalVars
{
    public static class GlobalVars
    {
        #region FileStorage
        public static readonly string Path = "C://WalletLog";
        #endregion

        #region JWT
        public static readonly string SecretKey =
           "6DA4AEB7-76BB-4D07-A022-CE50909725DD";
        public static readonly string ApiName = "WalletApi";
        public static readonly short ExpireDay = 360;

        #endregion

        #region MSSQL

        private static readonly string DatabaseName = "WalletDb";
        private static readonly string ServerName = ".";
        private static readonly string UserName = "sa";
        private static readonly string Password = "qwe123@@";


        public static readonly string ConnectionString =
            $"Data Source={ServerName};" +
            $"Initial Catalog={DatabaseName};" +
            $"User Id={UserName};" +
            $"Password={Password};";
        #endregion
    }
}
