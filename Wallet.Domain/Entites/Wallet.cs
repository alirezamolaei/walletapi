﻿using Wallet.Domain.Common;


namespace Wallet.Domain.Entites
{
    public class Wallet:BaseEntity
    {
        public Guid UserId { get; set; }
        public decimal Balance { get; set; }
    }
}
