﻿using Wallet.Domain.Common;

namespace Wallet.Domain.Entites
{
    public class User : BaseEntity
    {
        public string? Token { get; set; }
        public string Mobile { get; set; }
        public string? Password { get; set; }
        public int Score { get; set; }
    }
}
