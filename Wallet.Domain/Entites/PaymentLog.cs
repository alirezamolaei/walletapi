﻿using Wallet.Domain.Common;

namespace Wallet.Domain.Entites
{
    public class PaymentLog:BaseEntity
    {
        public Guid UserId { get; set; }
        public decimal Price { get; set; }
        public bool IsDone { get; set; }
        public string? StatusCode { get; set; }
        public string? PaymentCode { get; set; }

    }

}
