﻿using Wallet.Application.Utilitys;
using Wallet.Application.Utilitys.ApiResponse;
using Wallet.Domain.Interfaces.Identity;
using static Wallet.Domain.Enums.HttpStatusCode;

namespace Wallet.Application.Logics.UserLogic
{
    public class UserLogicMethods
    {
        private readonly IIdentityService _identityService;
        public UserLogicMethods(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        /// <summary>
        /// ورود کاربر
        /// </summary>
        public async Task<Response<SignInResponse>> SignInAsync(SignInRequest req)
        {
            try
            {
                //Create token and Get user
                var userInfo = await _identityService.AuthorizeAsync(req.Mobile);
                if (userInfo == null)
                    return Response<SignInResponse>.GetResponse<SignInResponse>(StatusCodesEnum.NotFound, null);

                var result = new SignInResponse();
                Tools.MapObj(userInfo, result);

                return Response<SignInResponse>.GetResponse(StatusCodesEnum.Successes, result);

            }
            catch (Exception ex)
            {
                return Response<SignInResponse>.GetResponse<SignInResponse>(StatusCodesEnum.Exception, null, ex);
            }
        }

        /// <summary>
        /// ثبت نام کاربر
        /// </summary>
        public void SignUp(SignUpRequest req)
        {
            //TODO: check data && create user 
        }
    }
}
