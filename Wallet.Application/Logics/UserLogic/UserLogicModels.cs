﻿namespace Wallet.Application.Logics.UserLogic
{
    public class SignUpRequest
    {
        public string UserName { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }

    }
    public class SignUpResponse
    {
        public string UserName { get; set; }
        public string Mobile { get; set; }

    }
    public class SignInRequest
    {
        public string Mobile { get; set; }
        public string Password { get; set; }
    }
    public class SignInResponse
    {
        public string UserName { get; set; }
        public string Mobile { get; set; }
        public string Token { get; set; }
    }


}
