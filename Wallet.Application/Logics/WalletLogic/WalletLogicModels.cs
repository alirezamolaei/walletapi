﻿namespace Wallet.Application.Logics.WalletLogic
{
    public class GetWalletResponse
    {
        public Guid UserId { get; set; }
        public Guid Guid { get; set; }
        public decimal Balance { get; set; }
    }
    public class UpdateWalletRequest
    {
        public decimal Balance { get; set; }
    }
}
