﻿using Wallet.Application.Utilitys;
using Wallet.Application.Utilitys.ApiResponse;
using Wallet.Domain.Interfaces.MSSQL;
using static Wallet.Domain.Enums.HttpStatusCode;

namespace Wallet.Application.Logics.WalletLogic
{
    public class WalletLogicMethods
    {
        readonly IUnitOfWork _unitOfWork;
        public WalletLogicMethods(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// دریافت اطلاعات کیف پول
        /// </summary>
        public async Task<Response<GetWalletResponse>> GetWallet(string userId)
        {
            try
            {

                var walletInfo = await _unitOfWork.Wallets.ReadAsync(z => z.UserId == Guid.Parse(userId));
                if (walletInfo == null)
                    return Response<GetWalletResponse>.GetResponse<GetWalletResponse>(StatusCodesEnum.NotFound, null);
                var result = new GetWalletResponse();
                Tools.MapObj(walletInfo, result);
                return Response<GetWalletResponse>.GetResponse(StatusCodesEnum.Successes, result);

            }
            catch (Exception ex)
            {
                return Response<GetWalletResponse>.GetResponse<GetWalletResponse>(StatusCodesEnum.Successes, null, ex);
            }
        }
        /// <summary>
        /// ویرایش اطلاعات کیف پول
        /// </summary>
        public async Task<Response<bool>> UpdateWallet(UpdateWalletRequest req,string userId)
        {
            try
            {
                var walletInfo = await _unitOfWork.Wallets.ReadAsync(z => z.UserId == Guid.Parse(userId));
                if (walletInfo == null)
                    return Response<bool>.GetResponse(StatusCodesEnum.NotFound, false);

                walletInfo.UpdateAt = DateTime.Now;
                walletInfo.Balance = req.Balance; //****If payment status is sucess Balance is updated
                _unitOfWork.Wallets.Update(walletInfo);
                //save change
                await _unitOfWork.SubmitChangeAsync();
                return Response<bool>.GetResponse(StatusCodesEnum.Successes, true);
                ///***Payment Callback Fill Payment Log
                //var logPayment = new PaymentLog()
                //{
                //    IsDone = false, or true 
                //    Price = req.Balance; 
                //    etc...
                //};

            }
            catch (Exception ex)
            {

                return Response<bool>.GetResponse(StatusCodesEnum.Successes, false, ex);
            }
        }
    }
}
