﻿namespace Wallet.Application.Utilitys
{
    public class Tools
    {
        public static void MapObj<TSource, TDestination>(TSource source, TDestination destination)
        {
            try
            {
                var sourceProperties = source?.GetType().GetProperties();
                var destinationProperties = destination?.GetType().GetProperties();

                foreach (var sourceProperty in sourceProperties)
                {
                    foreach (var destinationProperty in destinationProperties)
                    {
                        if (sourceProperty.Name == destinationProperty.Name && sourceProperty.PropertyType == destinationProperty.PropertyType)
                        {
                            destinationProperty.SetValue(destination, sourceProperty.GetValue(source));
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
