﻿using static Wallet.Domain.Enums.HttpStatusCode;

namespace Wallet.Application.Utilitys.ApiResponse
{
    public class ResponseError
    {

        #region Prop
        public string? Message { get; private set; }
        public int? Status { get; private set; }
        public bool IsOk { get; private set; }
        public StatusCodesEnum MessageType { get; private set; }
        #endregion


        public static List<ResponseError> Messages = new()
        {
            new ResponseError
            {
                Status = (int)StatusCodesEnum.Successes,
                Message = "Successes",
                IsOk = true,
                MessageType = StatusCodesEnum.Successes
            },
            new ResponseError
            {
                Status = (int)StatusCodesEnum.NotFound,
                Message = "NotFound",
                IsOk = false,
                MessageType = StatusCodesEnum.NotFound
            },
            new ResponseError
            {
                Status = (int)StatusCodesEnum.Forbidden,
                Message = "Forbidden",
                IsOk = false,
                MessageType = StatusCodesEnum.Forbidden
            },
            new ResponseError
            {
                Status = (int)StatusCodesEnum.Exception,
                Message = "Exception",
                IsOk = false,
                MessageType = StatusCodesEnum.Exception
            },

        };
    }
}
