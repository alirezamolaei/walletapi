﻿using Wallet.Infrastructure.Files;
using static Wallet.Domain.Enums.HttpStatusCode;

namespace Wallet.Application.Utilitys.ApiResponse
{
    public class Response<T>
    {
        #region Prop
        public string? Message { get; private set; }
        public bool? IsOk { get; private set; }
        public int? Status { get; private set; }
        public T? Data { get; private set; }
        public string? ExceptionDetail { get; private set; }
        #endregion

        public static Response<T> GetResponse<T>(StatusCodesEnum type, T data, Exception? ex = null)
        {
            var item = GetItem(type);
            var result = new Response<T>()
            {
                Data = data,
                Message = item.Message,
                ExceptionDetail = ex?.Message,
                Status = item.Status,
                IsOk = item.IsOk
            };
            if (ex is not null)
            {
                TextFileBuilder.WriteText(ex.Message + "\n" + ex.StackTrace);
            }
            return result;
        }

        private static ResponseError GetItem(StatusCodesEnum type) => ResponseError.Messages.FirstOrDefault(e => e.MessageType == type);

    }
}
